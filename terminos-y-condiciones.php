<?php
    include 'src/includes/header.php'
?>
    <main class="main-us">
        <section class="sct-banner-dtll pos-rel" id="section0">
            <img src="assets/images/banner/banner_politicas.jpg" alt="" class="img-cover">
            <div class="container content-title-banner">
                <h1 class="title-banner font-nexaheavy text-uppercase">términos y condiciones</h1>
            </div>
        </section>
        <section class="sct-tc">
            <div class="container">
                <div class="row">
                    <!-- ACCORDION TÉRMINOS Y CONDICIONES -->
                    <div class="col-xs-12 px-0">
                        <ul class="accordion">
                            <li class="list-accordion">
                                <a class="accordion-heading font-bold"><i class="icon-cerrar"></i>General
                                    information, scope</a>
                                <div class="accordion-content">
                                    <ul class="list-descr-detall">
                                        <li class="item-list-descr">These General Conditions of Sale (GCS) apply to
                                            all
                                            our business relationships with our Customers (hereinafter referred to
                                            as
                                            "Customer"). The GCS shall only apply if the Customer as a company
                                            (section
                                            14 of the German Civil Code [BGB]) is a legal entity under public law or
                                            a
                                            special fund under public law.</li>
                                        <li class="item-list-descr">Our deliveries and services are performed
                                            exclusively according to our GCS. The validity of any deviating general
                                            conditions of sale of the Customer is hereby expressly rejected, unless
                                            such
                                            condi-tions are recognised by us in writing. Our GCS shall also apply if
                                            we
                                            carry out the delivery without reservation, with knowledge of the
                                            Customer's
                                            conditions that conflict with or deviate from our GCS.</li>
                                        <li class="item-list-descr">Individual, one-off agreements made with the
                                            Customer (including supplements, addenda and amendments) shall always
                                            take
                                            precedence over these GCS. A written contract and/or our written
                                            confirmation is required for the content of such agreements.</li>
                                        <li class="item-list-descr">Legally relevant explanations and notices, which
                                            are
                                            to be submitted to us by the Customer upon conclusion of the contract
                                            (e.g.
                                            deadlines, notice of defects, declaration of withdrawal or reduction),
                                            must
                                            be made in writing to be effective.</li>
                                    </ul>
                                </div>
                            </li>
                            <li class="list-accordion">
                                <a class="accordion-heading font-bold"><i class="icon-cerrar"></i>Conclusion of
                                    contract</a>
                                <div class="accordion-content">
                                    <ul class="list-descr-detall">
                                        <li class="item-list-descr">Our offers are subject to confirmation and are
                                            non-binding. This also applies when we provide the Customer with
                                            catalogues,
                                            technical documentation (e.g. drawings, plans, evaluations,
                                            calculations,
                                            references to DIN standards), other product descriptions or documents,
                                            including those in electronic format, to which we reserve ownership and
                                            copyright; these may only be made available to third par-ties with prior
                                            written agreement.</li>
                                        <li class="item-list-descr">When a goods order is placed, the Customer
                                            enters a
                                            binding offer of contract. Unless indicated otherwise in the order, we
                                            are
                                            entitled to accept this offer of contract within two weeks of receipt.
                                        </li>
                                        <li class="item-list-descr">Acceptance can be notified to the Customer
                                            either in
                                            writing (e.g. through order confirmation) or through delivery of the
                                            goods.
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="list-accordion">
                                <a class="accordion-heading font-bold"><i class="icon-cerrar"></i>Prices and
                                    payment terms</a>
                                <div class="accordion-content">
                                    <ul class="list-descr-detall">
                                        <li class="item-list-descr">Unless indicated otherwise in the order
                                            confirmation, all prices are based on the acceptance of whole shipping
                                            units, net ex works or ex warehouse, plus the applicable statutory VAT.
                                        </li>
                                        <li class="item-list-descr">In general Payments are due with issuing of the
                                            invoice. Payments will always be allocated to set-tle the oldest account
                                            due.</li>
                                        <li class="item-list-descr">If the above payment deadline passes, the
                                            Customer
                                            is considered to be in arrears. In the event that the Customer is in
                                            arrears, the purchase price shall accrue interest at a rate of 9
                                            percentage
                                            points above the respective base rate. We reserve the right to assert
                                            further claims at any time due to arrears, including claims according to
                                            section 288 para. 5 of the BGB. With regard to traders, our right to
                                            commercial maturity interest (section 353 of the German Commercial Code
                                            [HGB]) remains unaf-fected.</li>
                                        <li class="item-list-descr">For a sale by delivery to a place other than the
                                            place of performance, the Customer shall bear the shipping costs from
                                            the
                                            warehouse and the costs of any shipping insurance required. Any duties,
                                            fees, taxes or other public charges shall be borne by the Customer.
                                            (5) Packaging is charged for at cost.</li>
                                        <li class="item-list-descr">The Customer shall have a right of set-off or
                                            right
                                            of retention only insofar as its claim has been established as final and
                                            absolute or is undisputed. In the event of defects in the delivery, the
                                            Custom-er's reciprocal rights shall remain unaffected, specifically in
                                            accordance with section 5 of these GCS.</li>
                                        <li class="item-list-descr">Should it become clear upon conclusion of the
                                            contract that our claim to payment of the purchase price is at risk due
                                            to
                                            the Customer's lack of financial capacity (e.g. an application to
                                            commence
                                            insolvency proceedings), we are entitled to withdraw from the contract –
                                            if
                                            applicable according to the deadline – in accordance with the statutory
                                            regulations on refusing performance. For contracts relating to the
                                            manufacture of unwarranted goods (custom-made items), we may declare our
                                            withdrawal immediately; the legal provisions relating to the
                                            dispensability
                                            of the deadline remain unaffected.</li>
                                    </ul>
                                </div>
                            </li>
                            <li class="list-accordion">
                                <a class="accordion-heading font-bold"><i class="icon-cerrar"></i>Time and late
                                    delivery</a>
                                <div class="accordion-content">
                                    <ul class="list-descr-detall">
                                        <li class="item-list-descr">Delivery deadlines are agreed individually or
                                            indicated by us upon acceptance of the order. If this is not the case,
                                            the
                                            delivery deadline will be approx. three weeks from the conclusion of the
                                            contract.</li>
                                        <li class="item-list-descr">In the event that we are unable to meet binding
                                            delivery deadlines for reasons beyond our control (non-availability of
                                            the
                                            service), we shall inform the Customer of this immediately and also of
                                            the
                                            new, expected delivery deadline. If the service is then not available
                                            within
                                            the new delivery deadline, we are entitled to withdraw from the
                                            contract,
                                            either in full or in part; we will immediately refund any return service
                                            provided by the Customer. A case of non-availability of the service in
                                            this
                                            sense, would in particular be the late delivery to us by our supplier if
                                            we
                                            have concluded a congruent covering transaction, neither us nor our
                                            supplier
                                            are at fault or we do not have a duty to supply in an individual case.
                                        </li>
                                        <li class="item-list-descr">The occurrence of late delivery by us is based
                                            on
                                            legal provisions. The Customer must provide a reminder in each case,
                                            however. If our delivery is late, the Customer can request lump sum
                                            compen-sation for the damages caused by the delay. The damage lump sum
                                            for
                                            each full calendar week of delay amounts to 0.5% of the net price
                                            (delivery
                                            value), however, totalling no more than 5% of the delivery value of the
                                            goods subject to late delivery. We reserve the right to prove that the
                                            customer has suffered no damage or significantly less damage than the
                                            aforementioned lump sum.</li>
                                        <li class="item-list-descr">The Customer's rights in accordance with
                                            sections 5,
                                            6 of these GCS and our legal rights in par-ticular in the event of an
                                            exclusion of the obligation to perform (e.g. due to impossibility or
                                            unacceptability of the delivery and/or subsequent fulfilment) remain
                                            unaffected.</li>
                                    </ul>
                                </div>
                            </li>
                            <li class="list-accordion">
                                <a class="accordion-heading font-bold"><i class="icon-cerrar"></i>Delivery, passing
                                    of risk, acceptance, default of acceptance</a>
                                <div class="accordion-content">
                                    <ul class="list-descr-detall">
                                        <li class="item-list-descr">The delivery is made ex warehouse, which is also
                                            the
                                            place of performance. Upon request by and at the cost of the Customer,
                                            the
                                            goods will be shipped to a different destination (sale by delivery to a
                                            place other than the place of performance). Unless agreed otherwise, we
                                            are
                                            entitled to determine the type of shipment (in particular forwarding
                                            agent,
                                            shipment route, packaging) ourselves.</li>
                                        <li class="item-list-descr">The risk of possible loss and possible
                                            deterioration
                                            of the goods is transferred to the Customer at the handover stage at the
                                            latest. In the event of sale by delivery to a place other than the place
                                            of
                                            performance, however, the risk of possible loss or of possible
                                            deterioration
                                            of the goods and the risk of delay is transferred earlier, at the
                                            delivery
                                            stage of the goods, to the forwarding agent, the freight carrier or
                                            other
                                            person nominated to carry out the dispatch. Should acceptance be agreed,
                                            this is authoritative for the transfer of risk. The same applies for
                                            handover/acceptance if the customer is in default of acceptance.</li>
                                        <li class="item-list-descr">The Customer is not permitted to refuse
                                            acceptance
                                            of the goods due to minor defects.</li>
                                        <li class="item-list-descr">If the Customer is in default of acceptance,
                                            fails
                                            to cooperate or if our delivery is delayed due to other reasons for
                                            which
                                            the Customer is to blame, we are entitled to request reimbursement for
                                            the
                                            damages caused by this including additional expenditure.</li>
                                    </ul>
                                </div>
                            </li>
                            <li class="list-accordion">
                                <a class="accordion-heading font-bold"><i class="icon-cerrar"></i>Warranty</a>
                                <div class="accordion-content">
                                    <ul class="list-descr-detall">
                                        <li class="item-list-descr">The provisions of the law apply to the rights of
                                            the
                                            Customer in the event of material defects and defects of title
                                            (including
                                            incorrect and shortfall in delivery), insofar as nothing is stated to
                                            the
                                            contrary below.</li>
                                        <li class="item-list-descr">Our liability for defects is based above all on
                                            the
                                            agreement concluded on the condition of the goods. If no condition has
                                            been
                                            agreed, statutory rule must be used to determine whether or not a defect
                                            is
                                            present. We shall not accept liability for any public statements.
                                            Defects do
                                            not include natural wear and tear or damage that occurs after the
                                            transfer
                                            of risk due to incorrect or negligent operation, excessive strain or
                                            damage
                                            that occurs due to specific external factors that are not presupposed by
                                            the
                                            contract.</li>
                                        <li class="item-list-descr">The Customer's claims for defects assume that
                                            the
                                            Customer has fulfilled its legal duties to inspection and objection. In
                                            the
                                            event that a defect is found upon inspection or at a later stage, we
                                            must be
                                            notified of this immediately in writing. The notification is deemed
                                            immediate if it is made within two weeks, whereby the deadline is
                                            considered
                                            met if the notification is sent off in time. Irrespective of this duty
                                            to
                                            inspection and objection, the Customer must notify us in writing of
                                            obvious
                                            defects (including incorrect and shortfall in delivery) within two weeks
                                            of
                                            delivery, whereby the deadline is also considered met if the
                                            notification is
                                            sent off in time. If the customer fails to carry out the correct
                                            inspection
                                            and/or defect notification, we shall not be liable for any defects that
                                            are
                                            not notified to us.</li>
                                        <li class="item-list-descr">If the delivered goods are faulty, the Customer
                                            may
                                            initially request rectification of the defect (sub-sequent improvement)
                                            as
                                            the subsequent fulfilment, unless the delivery of defect-free goods
                                            (subse-quent delivery) is absolutely necessary for the Customer and can
                                            be
                                            carried out by us at reasonable costs. We must be given suitable time
                                            and
                                            opportunity for the subsequent fulfilment. If this option is rejected,
                                            we
                                            shall be released from subsequent fulfilment and further claims for
                                            defects.
                                        </li>
                                        <li class="item-list-descr">We are entitled to make the owed subsequent
                                            fulfilment dependent upon the Customer paying the purchase price due.
                                            However, the Customer is entitled to be refunded an appropriate share of
                                            the
                                            purchase price in relation to the defect.</li>
                                        <li class="item-list-descr">The expenditure required for the purpose of
                                            inspection and subsequent fulfilment, in particular the costs of
                                            transport,
                                            travel, work and materials (not dismantling and assembly costs) shall be
                                            borne by us if a defect does, in fact, exist. However, if a Customer
                                            request
                                            to rectify a defect is deemed unjustified, we may request that the
                                            Customer
                                            pays for the costs incurred for this.</li>
                                        <li class="item-list-descr">If the subsequent fulfilment fails or a
                                            reasonable
                                            deadline set by the Customer for the subsequent fulfilment passes
                                            without
                                            success or is legally superfluous, the Customer may withdraw from the
                                            sales
                                            agreement or reduce the purchase price. A right to withdraw does not
                                            exist
                                            in the event of minor defects.</li>
                                        <li class="item-list-descr">Customer claims for compensation for damages
                                            exist
                                            only in accordance with section 7 and are otherwise excluded.</li>
                                    </ul>
                                </div>
                            </li>
                            <li class="list-accordion">
                                <a class="accordion-heading font-bold"><i class="icon-cerrar"></i>Other
                                    liability</a>
                                <div class="accordion-content">
                                    <ul class="list-descr-detall">
                                        <li class="item-list-descr">Insofar as nothing else arises from these GCS
                                            including the following provisions, we shall be liable in the event of a
                                            breach of contractual and non-contractual duties in accordance with the
                                            applicable statutory provisions.</li>
                                        <li class="item-list-descr">We shall be liable for compensation for damages
                                            –
                                            for whatever legal reason – in the case of intent and gross negligence.
                                            In
                                            cases of minor negligence we shall only be liable:
                                            <ul class="list-int-tc">
                                                <li>for damages arising from injury to life, body or health</li>
                                                <li>for damages arising from the breach of an essential contractual
                                                    duty
                                                    (an obligation where fulfilment is essential to the proper
                                                    carrying
                                                    out of the contract and which the contracting partner often
                                                    relies
                                                    on and may rely on); in this case, however, our liability is
                                                    restricted to reimbursement of foreseeable, typically occurring
                                                    damage.</li>
                                            </ul>
                                        </li>
                                        <li class="item-list-descr">The liability restrictions of para. 2 do not
                                            apply
                                            if we have maliciously concealed a defect or have adopted a warranty for
                                            the
                                            condition of the goods. The same applies for Customer claims according
                                            to
                                            the German Product Liability Act (Produkthaftungsgesetz).</li>
                                        <li class="item-list-descr">In the event of a breach of duty that does not
                                            produce a defect, the Customer may only withdraw from or terminate the
                                            contract if we are responsible for the breach of duty. The Customer's
                                            free
                                            right to termination is excluded.</li>
                                    </ul>
                                </div>
                            </li>
                            <li class="list-accordion">
                                <a class="accordion-heading font-bold"><i class="icon-cerrar"></i>Reservation of
                                    title</a>
                                <div class="accordion-content">
                                    <ul class="list-descr-detall">
                                        <li class="item-list-descr">We reserve the title of ownership of the goods
                                            sold
                                            until full payment of all our current and future claims from the sales
                                            agreement and an ongoing business relationship (secured claims) has been
                                            made.</li>
                                        <li class="item-list-descr">The goods under reservation of title may not be
                                            pledged to third parties or reassigned by way of security until the
                                            secured
                                            claims have been paid in full. The Customer must notify us immediately
                                            in
                                            writing if and the extent to which third parties attempt to seize the
                                            goods
                                            belonging to us.</li>
                                        <li class="item-list-descr">Should the Customer act in breach of contract,
                                            specifically in the event of non-payment of the purchase price due, we
                                            have
                                            the right, according to legal provisions, to withdraw from the contract
                                            and
                                            to demand the return of goods based on the reservation of title and the
                                            withdrawal. If the customer does not pay the purchase price due, we may
                                            only
                                            assert these rights if we have given the customer a reasonable time
                                            frame in
                                            which to make the payment and payment has not been made or such a
                                            deadline
                                            is superfluous according to the legal provisions.</li>
                                        <li class="item-list-descr">The Customer is authorised to sell and/or
                                            process
                                            the goods subject to reservation of title in the course of normal
                                            business.
                                            In this case, the following provisions shall also apply.
                                            <ul class="list-int-tc">
                                                <li>The reservation of title extends to the products created through
                                                    processing, mixing or connecting our goods, at their full value,
                                                    with us as the manufacturer. If, in the event of processing,
                                                    mixing
                                                    or connecting with third-party goods, their reservation of title
                                                    remains, we shall acquire joint ownership with regard to the
                                                    invoice
                                                    value of the processed, mixed or connected goods. In addition,
                                                    the
                                                    resulting product is subject to the same conditions as the goods
                                                    delivered under reservation of title.</li>
                                                <li>The Customer hereby assigns to us, by way of security, any and
                                                    all
                                                    claims against third parties from the resale of goods or
                                                    products in
                                                    full and to the extent of any joint ownership in accordance with
                                                    the
                                                    preceding paragraph. We hereby accept the assignment. The
                                                    Customer's
                                                    duties set out in para. 2 also apply in view of the assigned
                                                    claims.
                                                </li>
                                                <li>The Customer shall remain entitled to collect the claim
                                                    alongside
                                                    us. We undertake not to collect the claim provided the Customer
                                                    fulfils its payment obligations towards us, does not run into
                                                    arrears, has not filed an application to commence insolvency
                                                    proceedings and shows no other lack of financial capacity.
                                                    However,
                                                    should any of these apply, we may request that the Customer
                                                    informs
                                                    us of the asserted claims and their debtors, provides all the
                                                    information required for collection, surrenders all related
                                                    documentation and informs the debtors (third parties) of the
                                                    assignment.</li>
                                                <li>If the achievable value of the securities exceeds our claims by
                                                    more
                                                    than 10%, upon request by the Customer we will release
                                                    securities of
                                                    our choice.</li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="list-accordion">
                                <a class="accordion-heading font-bold"><i class="icon-cerrar"></i>Statute of
                                    limitation</a>
                                <div class="accordion-content">
                                    <ul class="list-descr-detall">
                                        <li class="item-list-descr">Notwithstanding section 438 para. 1 no. 3 of the
                                            BGB, the general statutory period of limitation for claims from material
                                            defects and defects of title is one year from delivery. Provided
                                            acceptance
                                            has been agreed, the statute of limitation commences at the time of
                                            acceptance.</li>
                                        <li class="item-list-descr">Special legal regulations shall remain
                                            unaffected
                                            for third-party claims for return in rem (section 438 para. 1 no. 1 of
                                            the
                                            BGB), in cases of fraudulent intent (section 438 para. 3 of the BGB) and
                                            for
                                            claims of recourse against the supplier in final delivery to the
                                            consumer
                                            (section 479 of the BGB).</li>
                                        <li class="item-list-descr">The above statutory periods of limitation of the
                                            law
                                            governing the sale of goods also apply to contractual and
                                            non-contractual
                                            Customer claims for damages that are based on goods defects, unless the
                                            application of the standard statute of limitation (sections 195, 199 of
                                            the
                                            BGB) would lead to shorter statute of limitation in an individual case.
                                            The
                                            statutory periods of limitation of the German Product Liability Act
                                            remain
                                            unaffected in each case. Otherwise, the statutory periods of limitation
                                            apply exclusively for Customer claims for damages.</li>
                                    </ul>
                                </div>
                            </li>
                            <li class="list-accordion">
                                <a class="accordion-heading font-bold"><i class="icon-cerrar"></i>Choice of law and
                                    place of jurisdiction</a>
                                <div class="accordion-content">
                                    <p class="p-internas">All legal relationships are subject to the law of the
                                        Federal Republic of Germany. The United Nations Convention on Contracts for
                                        the
                                        International Sale of Goods does not apply. Insofar as the Customer is a
                                        registered trader or a legal entity under public law, the place of
                                        jurisdiction
                                        shall be our place of business; however, we have the right to also take
                                        legal
                                        action against the Customer at its place of residence.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </main>
<?php
    include 'src/includes/footer.php'
?>
    <!-- <script>
            $(document).ready(function(){
                $(".list-nav .item-nav:first-child").hover(function() {
                   

                    if ($('.content-nav').attr('class') == 'bg-salud') {
                        $(".menu-salud").css("opacity","1");
                        $('.content-nav').addClass('bg-salud');

                    }else{
                        $('.content-nav').removeClass('bg-salud')
                    }
                    
                });
                $(".list-nav .item-nav:nth-child(2)").hover(function() {
                    $(".content-nav").addClass('bg-bienestar');
                    $(".menu-bienestar").css("opacity","1")
                });
                $(".list-nav .item-nav:nth-child(3)").hover(function() {
                    $(".content-nav").addClass('bg-belleza');
                    $(".menu-belleza").css("opacity","1")
                });
                $(".list-nav .item-nav:nth-child(4)").hover(function() {
                    $(".content-nav").addClass('bg-actividad');
                    $(".menu-actividad").css("opacity","1")
                });
                $(".list-nav .item-nav:nth-child(5)").hover(function() {
                    $(".content-nav").addClass('bg-linea-bb');
                    $(".menu-linea-bb").css("opacity","1")
                });   
            });
        </script> -->

</body>

</html>