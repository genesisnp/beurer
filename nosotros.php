<?php
    include 'src/includes/header.php'
?>
    <main class="main-us">
        <section class="sct-banner-int pos-rel" id="section0">
            <img src="assets/images/banner/nosotros.jpg" alt="" class="img-cover">
            <div class="container content-title-banner">
                <h1 class="title-banner font-nexaheavy text-uppercase">Nosotros</h1>
            </div>
        </section>
        <section class="sct-description">
            <div class="container">
                <div class="row animatedParent animateOnce">
                    <div class="col-xs-12 col-md-6 animated fadeInLeftShort">
                        <div class="row">
                            <div class="col-xs-12 col-sm-11">
                                <h2 class="title-border text-uppercase font-nexaheavy">Déjese inspirar por nuestras
                                    novedades y disfrute de pura vitalidad con beurer</h2>
                            </div>
                            <div class="description-us col-xs-12 col-sm-11">
                                <p class="p-internas">Desde 1919 y con un surtido que abarca más de 500 productos
                                    mantenemos la promesa de nuestro eslogan: salud y bienestar. Nuestras gamas de
                                    productos se ocupan de su total bienestar.</p>
                                <p class="p-internas">Con productos médicos especializados en la prevención y el
                                    diagnóstico, productos de peso y diagnóstico, así como aparatos de masaje y terapia
                                    en la categoría del bienestar y nuestro apartado para la vida activa con sensores de
                                    actividad y pulsómetros de frecuencia cardiaca, le ofrecemos todo lo que necesita
                                    para un estilo de vida moderno y saludable. En los segmentos Beauty y Babycare
                                    encontrará además productos profesionales que le acompañarán día a día.</p>
                                <p class="p-internas">Gracias a la constante adaptación a las nuevas tendencias y a la
                                    observación de las necesidades de nuestros clientes, contamos con una gama completa
                                    de productos que incluye desde los modelos más básicos hasta productos de alta gama.
                                    Trabajamos en colaboración con renombrados institutos, socios industriales y
                                    asesores.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 animated growIn">
                        <div class="img-us-int">
                            <img src="assets/images/nosotros-int.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>

</body>

</html>