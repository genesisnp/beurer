<?php
    include 'src/includes/header.php'
?>
    <main class="main-home" id="fullpage">
        <section class="section sct-video-home pos-rel">
            <source>
                <video autoplay loop muted src="assets/video/video_1.mp4" class="video hidden-xs hidden-sm"></video>
            </source>
            <div class="text-center title-banner-home hidden-xs hidden-sm">
                <h2 class="title-b-products font-nexaheavy">#SaludYBienestar</h2>
            </div>
            <div class="slider-home visible-xs visible-sm">
                <div class="item-slider-home">
                    <figure>
                        <img src="assets/images/banner/movil-salud.jpg" alt="">
                    </figure>
                    <div class="name-prod-slider">
                        <h2 class="title-slider-h">salud</h2>
                        <a href="salud.php" class="btn-slider-h bg-salud">ver detalles</a>
                    </div>
                    <div class="degradado"></div>
                </div>
                <div class="item-slider-home">
                    <figure>
                        <img src="assets/images/banner/movil-bienestar.jpg" alt="">
                    </figure>
                    <div class="name-prod-slider">
                        <h2 class="title-slider-h">bienestar</h2>
                        <a href="bienestar.php" class="btn-slider-h bg-bienestar">ver detalles</a>
                    </div>
                    <div class="degradado"></div>
                </div>
                <div class="item-slider-home">
                    <figure>
                        <img src="assets/images/banner/movil-belleza.jpg" alt="">
                    </figure>
                    <div class="name-prod-slider">
                        <h2 class="title-slider-h">belleza</h2>
                        <a href="belleza.php" class="btn-slider-h bg-belleza">ver detalles</a>
                    </div>
                    <div class="degradado"></div>
                </div>
                <div class="item-slider-home">
                    <figure>
                        <img src="assets/images/banner/movil-actividad.jpg" alt="">
                    </figure>
                    <div class="name-prod-slider">
                        <h2 class="title-slider-h">actividad</h2>
                        <a href="actividad.php" class="btn-slider-h bg-actividad">ver detalles</a>
                    </div>
                    <div class="degradado"></div>
                </div>
                <div class="item-slider-home">
                    <figure>
                        <img src="assets/images/banner/movil-bebe.jpg" alt="">
                    </figure>
                    <div class="name-prod-slider">
                        <h2 class="title-slider-h">linea bebé</h2>
                        <a href="linea-bebe.php" class="btn-slider-h bg-linea-bb">ver detalles</a>
                    </div>
                    <div class="degradado"></div>
                </div>
            </div>
            
        </section>
        <!-- CARROSUEL PRODUCTOS DESTACADOS -->
        <section class="section sct-two-home">
            <h2 class="ttl-prd-dst text-center font-nexaheavy visible-xs visible-sm ">PRODUCTOS DESTACADOS</h2>
            <div class="container">
                <div class="row">
                    
                    <div class="carousel-home-general">
                        <div class="item-carosuel">
                            <div class="container">
                                <div class="info-slider-home">
                                    <button class="trans-info name-product bg-belleza font-nexaregular">Belleza</button>
                                    <h2 class="trans-info title-big-slider font-nexaheavy">FC 96 Pureo Intense Cleansing</h2>
                                    <span class="trans-info subtile-slider text-uppercase font-nexaregular">cepillo facial</span>
                                    <p class="trans-info description-slider font-nexaregular">Limpieza delicada y en profundidad para lucir un cutis
                                        suave y radiante. Gracias a su pantalla LCD, este cepillo facial le indicará
                                        siempre el estado de la batería, el cambio de accesorios y la velocidad.</p>
                                    <a href="#" class="trans-info btn-slider bg-belleza">VER PRODUCTO</a>
                                </div>
                                <div class="img-slider-h">
                                    <img src="assets/images/productos/producto1.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="item-carosuel">
                            <div class="container">
                                <div class="info-slider-home">
                                    <button class="trans-info name-product bg-belleza font-nexaregular">Belleza</button>
                                    <h2 class="trans-info title-big-slider font-nexaheavy">FC 96 Pureo Intense Cleansing</h2>
                                    <span class="trans-info subtile-slider text-uppercase font-nexaregular">cepillo facial</span>
                                    <p class="trans-info description-slider font-nexaregular">Limpieza delicada y en profundidad para lucir un cutis
                                        suave y radiante. Gracias a su pantalla LCD, este cepillo facial le indicará
                                        siempre el estado de la batería, el cambio de accesorios y la velocidad.</p>
                                    <a href="#" class="trans-info btn-slider bg-belleza">VER PRODUCTO</a>
                                </div>
                                <div class="img-slider-h">
                                    <img src="assets/images/productos/producto1.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="item-carosuel">
                            <div class="container">
                                <div class="info-slider-home">
                                    <button class="trans-info name-product bg-belleza font-nexaregular">Belleza</button>
                                    <h2 class="trans-info title-big-slider font-nexaheavy">FC 96 Pureo Intense Cleansing</h2>
                                    <span class="trans-info subtile-slider text-uppercase font-nexaregular">cepillo facial</span>
                                    <p class="trans-info description-slider font-nexaregular">Limpieza delicada y en profundidad para lucir un cutis
                                        suave y radiante. Gracias a su pantalla LCD, este cepillo facial le indicará
                                        siempre el estado de la batería, el cambio de accesorios y la velocidad.</p>
                                    <a href="#" class="trans-info btn-slider bg-belleza">VER PRODUCTO</a>
                                </div>
                                <div class="img-slider-h">
                                    <img src="assets/images/productos/cepillo.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="item-carosuel">
                            <div class="container">
                                <div class="info-slider-home">
                                    <button class="trans-info name-product bg-belleza font-nexaregular">Belleza</button>
                                    <h2 class="trans-info title-big-slider font-nexaheavy">FC 96 Pureo Intense Cleansing</h2>
                                    <span class="trans-info subtile-slider text-uppercase font-nexaregular">cepillo facial</span>
                                    <p class="trans-info description-slider font-nexaregular">Limpieza delicada y en profundidad para lucir un cutis
                                        suave y radiante. Gracias a su pantalla LCD, este cepillo facial le indicará
                                        siempre el estado de la batería, el cambio de accesorios y la velocidad.</p>
                                    <a href="#" class="trans-info btn-slider bg-belleza">VER PRODUCTO</a>
                                </div>
                                <div class="img-slider-h">
                                    <img src="assets/images/productos/producto1.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section sct-three-home">
            <div class="container-fluid">
                <div class="row">
                    <div class="carrosuel-two-home">
                        <div class="wrapper-cards-products">
                            <div class="content-img-card">
                                <img src="assets/images/productos/producto1.jpg" alt="" class="img-cnt">
                            </div>
                            <div class="content-title-card">
                                <h2 class="h2-title font-nexaheavy">FC 96 pureo intense cleansing</h2>
                                <span class="d-block subtitle-card">sauna facial</span>
                                <div class="btn-vp">
                                    <a href="detalle-de-producto.php" class="a-btn font-nexaheavy">ver producto</a>
                                </div>
                            </div>
                        </div>
                        <div class="wrapper-cards-products">
                            <div class="content-img-card">
                                <img src="assets/images/productos/producto2.jpg" alt="" class="img-cnt">
                            </div>
                            <div class="content-title-card">
                                <h2 class="h2-title font-nexaheavy">FC 96 pureo intense cleansing</h2>
                                <span class="d-block subtitle-card">sauna facial</span>
                                <div class="btn-vp">
                                    <a href="detalle-de-producto.php" class="a-btn font-nexaheavy">ver producto</a>
                                </div>
                            </div>
                        </div>
                        <div class="wrapper-cards-products">
                            <div class="content-img-card">
                                <img src="assets/images/productos/producto3.jpg" alt="" class="img-cnt">
                            </div>
                            <div class="content-title-card">
                                <h2 class="h2-title font-nexaheavy">FC 96 pureo intense cleansing</h2>
                                <span class="d-block subtitle-card">sauna facial</span>
                                <div class="btn-vp">
                                    <a href="detalle-de-producto.php" class="a-btn font-nexaheavy">ver producto</a>
                                </div>
                            </div>
                        </div>
                        <div class="wrapper-cards-products">
                            <div class="content-img-card">
                                <img src="assets/images/productos/producto1.jpg" alt="" class="img-cnt">
                            </div>
                            <div class="content-title-card">
                                <h2 class="h2-title font-nexaheavy">FC 96 pureo intense cleansing</h2>
                                <span class="d-block subtitle-card">sauna facial</span>
                                <div class="btn-vp">
                                    <a href="detalle-de-producto.php" class="a-btn font-nexaheavy">ver producto</a>
                                </div>
                            </div>
                        </div>
                        <div class="wrapper-cards-products">
                            <div class="content-img-card">
                                <img src="assets/images/productos/producto1.jpg" alt="" class="img-cnt">
                            </div>
                            <div class="content-title-card">
                                <h2 class="h2-title font-nexaheavy">FC 96 pureo intense cleansing</h2>
                                <span class="d-block subtitle-card">sauna facial</span>
                                <div class="btn-vp">
                                    <a href="detalle-de-producto.php" class="a-btn font-nexaheavy">ver producto</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section fp-auto-height">
            <?php
                include 'src/includes/footer.php'
            ?>
        </section>
    </main>

    
    <script src="assets/js/libraries/fullpage.js"></script>
    <script>
        if (screen && screen.width > 1300) {
            let fullpageDiv = $('#fullpage');
            if (fullpageDiv.length) {
                fullpageDiv.fullpage({
                    scrollBar: true,
                    scrollOverflow: true,
                    verticalCentered: true,
                    afterRender: function () {
                    }
                });
            }
        }
    </script>

</body>

</html>