<?php
    include 'src/includes/header.php'
?>
    <main class="main-us">
        <section class="sct-banner-int pos-rel" id="section0">
            <img src="assets/images/banner/instrucciones-de-uso.jpg" alt="" class="img-cover">
            <div class="container content-title-banner">
                <h1 class="title-banner font-nexaheavy text-uppercase">instrucciones de uso</h1>
            </div>
        </section>
        <section class="sct-inst-use">
            <div class="container">
                <div class="row">
                    <!-- BREADCRUMB -->
                    <div class="col-xs-12">
                        <ol class="breadcrumb row">
                            <li class="item-bradcrumb"><a href="#" class="link-bradcrumb p-internas">Home</a></li>
                            <li class="item-bradcrumb"><a href="#"
                                    class="link-bradcrumb p-internas active">Instrucciones de uso</a></li
                                class="item-bradcrumb">
                        </ol>
                    </div>
                    <!-- TITLE / DESCRIPTION PAGE -->
                    <div class="col-xs-12">
                        <div class="row animatedParent animateOnce" data-sequence='500'>
                            <div class="col-xs-12 col-sm-6 col-md-5 animated fadeInLeftShort" data-id="1">
                                <h2 class="title-border text-uppercase font-nexaheavy">Instrucciones para<br> productos
                                    beurer</h2>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-5 animated growIn slowest" data-id="2">
                                <p class="p-internas">Aquí podrá descargar las instrucciones de uso de su producto de
                                    Beurer en formato PDF, Beurer cuenta con formatos de ayuda para todas las
                                    necesidades: desde alimentación y peso hasta presión arterial y análisis del sueño,
                                    entre otras.</p>
                            </div>
                        </div>
                    </div>
                    <!-- FILTER QA -->
                    <div class="col-xs-12 col-md-10 wrapper-select animatedParent animateOnce"  data-sequence='500'>
                        <div class="dropdown-select animated fadeInLeftShort" data-id='1'>
                            <select id="select-stores" class="font-light">
                                <option>Categoría de producto</option>
                                <option value="salud" class="stores">Línea Salud</option>
                                <option value="bienestar" class="stores">Línea Bienestar</option>
                                <option value="actividad" class="stores">Línea actividad</option>
                                <option value="linea-bb" class="stores">Línea Bebé</option>
                                <option value="app" class="stores">aplicaciones</option>
                            </select>
                        </div>

                        <div class="dropdown-select animated fadeInLeftShort" data-id='2'>
                            <select id="select-stores" class="font-light">
                                <option>Sub categoría de producto</option>
                                <option value="magdalena" class="stores">depilacion 2</option>
                            </select>
                        </div>
                        <div class="btn-bsc animated fadeInLeftShort" data-id='3'>
                            <button class="font-nexaheavy">BUSCAR</button>
                        </div>
                    </div>
                    <!-- RESPUESTA DE SELECT -->
                    <div class="col-xs-12 rsp-slct">
                        <div class="row animatedParent animateOnce"  data-sequence='500'>
                            <div class="col-xs-12 col-sm-6 col-md-3 animated fadeInLeftShort" data-id='1'>
                                <h3 class="titles-int-sl">masajes<br> para pies</h3>
                                <p class="p-internas-sl">Deléitese con un masaje de pies relajante para las piernas
                                    cansadas y pesadas.</p>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-9 animated fadeInRightShort" data-id='2'>
                                <div class="row">
                                    <div class="col-xs-12 col-md-11 float-r">
                                        <div class="slider-instr-use slider-dscg">
                                            <div class="wrapper-cards-products">
                                                <div class="content-img-card">
                                                    <img src="assets/images/productos/producto1.jpg" alt=""
                                                        class="img-cnt">
                                                </div>
                                                <div class="content-title-card">
                                                    <h2 class="h2-title font-nexaheavy">FC 96</h2>
                                                    <span class="d-block subtitle-card">sauna facial</span>
                                                    <div class="btn-dscg">
                                                        <a href="#" class="a-btn font-nexaheavy">descargar</a>
                                                    </div>
                                                </div>
                                                <a href="#" class="icon-dscg-use icon-descargar"></a>
                                            </div>
                                            <div class="wrapper-cards-products">
                                                <div class="content-img-card">
                                                    <img src="assets/images/productos/producto1.jpg" alt=""
                                                        class="img-cnt">
                                                </div>
                                                <div class="content-title-card">
                                                    <h2 class="h2-title font-nexaheavy">FC 96</h2>
                                                    <span class="d-block subtitle-card">sauna facial</span>
                                                    <div class="btn-dscg">
                                                        <a href="#" class="a-btn font-nexaheavy">descargar</a>
                                                    </div>
                                                </div>
                                                <a href="#" class="icon-dscg-use icon-descargar"></a>
                                            </div>
                                            <div class="wrapper-cards-products">
                                                <div class="content-img-card">
                                                    <img src="assets/images/productos/producto1.jpg" alt=""
                                                        class="img-cnt">
                                                </div>
                                                <div class="content-title-card">
                                                    <h2 class="h2-title font-nexaheavy">FC 96</h2>
                                                    <span class="d-block subtitle-card">sauna facial</span>
                                                    <div class="btn-dscg">
                                                        <a href="#" class="a-btn font-nexaheavy">descargar</a>
                                                    </div>
                                                </div>
                                                <a href="#" class="icon-dscg-use icon-descargar"></a>
                                            </div>
                                            <div class="wrapper-cards-products">
                                                <div class="content-img-card">
                                                    <img src="assets/images/productos/producto1.jpg" alt=""
                                                        class="img-cnt">
                                                </div>
                                                <div class="content-title-card">
                                                    <h2 class="h2-title font-nexaheavy">FC 96</h2>
                                                    <span class="d-block subtitle-card">sauna facial</span>
                                                    <div class="btn-dscg">
                                                        <a href="#" class="a-btn font-nexaheavy">descargar</a>
                                                    </div>
                                                </div>
                                                <a href="#" class="icon-dscg-use icon-descargar"></a>
                                            </div>
                                            <div class="wrapper-cards-products">
                                                <div class="content-img-card">
                                                    <img src="assets/images/productos/producto1.jpg" alt=""
                                                        class="img-cnt">
                                                </div>
                                                <div class="content-title-card">
                                                    <h2 class="h2-title font-nexaheavy">FC 96</h2>
                                                    <span class="d-block subtitle-card">sauna facial</span>
                                                    <div class="btn-dscg">
                                                        <a href="#" class="a-btn font-nexaheavy">descargar</a>
                                                    </div>
                                                </div>
                                                <a href="#" class="icon-dscg-use icon-descargar"></a>
                                            </div>
                                            <div class="wrapper-cards-products">
                                                <div class="content-img-card">
                                                    <img src="assets/images/productos/producto1.jpg" alt=""
                                                        class="img-cnt">
                                                </div>
                                                <div class="content-title-card">
                                                    <h2 class="h2-title font-nexaheavy">FC 96</h2>
                                                    <span class="d-block subtitle-card">sauna facial</span>
                                                    <div class="btn-dscg">
                                                        <a href="#" class="a-btn font-nexaheavy">descargar</a>
                                                    </div>
                                                </div>
                                                <a href="#" class="icon-dscg-use icon-descargar"></a>
                                            </div>
                                            <div class="wrapper-cards-products">
                                                <div class="content-img-card">
                                                    <img src="assets/images/productos/producto1.jpg" alt=""
                                                        class="img-cnt">
                                                </div>
                                                <div class="content-title-card">
                                                    <h2 class="h2-title font-nexaheavy">FC 96</h2>
                                                    <span class="d-block subtitle-card">sauna facial</span>
                                                    <div class="btn-dscg">
                                                        <a href="#" class="a-btn font-nexaheavy">descargar</a>
                                                    </div>
                                                </div>
                                                <a href="#" class="icon-dscg-use icon-descargar"></a>
                                            </div>
                                            <div class="wrapper-cards-products">
                                                <div class="content-img-card">
                                                    <img src="assets/images/productos/producto1.jpg" alt=""
                                                        class="img-cnt">
                                                </div>
                                                <div class="content-title-card">
                                                    <h2 class="h2-title font-nexaheavy">FC 96</h2>
                                                    <span class="d-block subtitle-card">sauna facial</span>
                                                    <div class="btn-dscg">
                                                        <a href="#" class="a-btn font-nexaheavy">descargar</a>
                                                    </div>
                                                </div>
                                                <a href="#" class="icon-dscg-use icon-descargar"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>
    <?php
        include 'src/includes/footer.php'
    ?>

</body>

</html>