<?php
    include 'src/includes/header.php'
?>
    <main class="main-us">
        <section class="sct-banner-dtll pos-rel" id="section0">
            <img src="assets/images/banner/banner_privacidad_datos.jpg" alt="" class="img-cover">
            <div class="container content-title-banner">
                <h1 class="title-banner font-nexaheavy text-uppercase">políticas de privacidad</h1>
            </div>
        </section>
        <section class="sct-polit">
            <div class="container">
                <div class="row">
                    <!-- ACCORDION TÉRMINOS Y CONDICIONES -->
                    <div class="col-xs-12">
                        <h2 class="font-nexaeavy">Declaración sobre protección de datos</h2>
                        <p class="p-internas">Le agradecemos su interés por nuestra empresa. La protección de datos es
                            un tema de vital importancia para la gerencia de la Beurer GmbH. Por lo general, se puede
                            usar el sitio web de Beurer GmbH sin introducir todos los datos personales. Sin embargo, si
                            una persona afectada desea usar servicios especiales de nuestra empresa mediante el sitio
                            web, puede ser necesario el procesamiento de datos de carácter personal. En caso de que el
                            procesamiento de datos personales sea obligatorio y no exista una base jurídica para dicho
                            procesamiento, generalmente solicitaremos el consentimiento de la persona afectada.</p>
                        <p class="p-internas">El procesamiento de datos de carácter personal, como el nombre, la
                            dirección postal, la dirección de correo electrónico o el número de teléfono de una persona
                            afectada, se realiza siempre conforme al Reglamento de base sobre protección de datos y en
                            virtud de las disposiciones en materia de protección de datos específicas del país que son
                            aplicables a Beurer GmbH. Mediante la presente declaración sobre protección de datos,
                            nuestra empresa desea informar al público sobre el tipo, el alcance y la finalidad de los
                            datos de carácter personal que recopilamos, utilizamos y procesamos. Además, mediante la
                            presente declaración sobre protección de datos se informa a las personas afectadas de los
                            derechos que les corresponden.</p>
                        <p class="p-internas">Beurer GmbH ha establecido numerosas medidas técnicas y organizativas para
                            los responsables del procesamiento con el fin de garantizar una protección lo más completa
                            posible de los datos de carácter personal que se procesan. No obstante, en las
                            transferencias de datos a través de Internet puede haber brechas de seguridad inherentes,
                            por lo que no se puede garantizar una protección absoluta.</p>
                    </div>
                    <div class="col-xs-12 px-0">
                        <ul class="accordion">
                            <li class="list-accordion">
                                <a class="accordion-heading font-bold"><i class="icon-cerrar"></i>Nombre y dirección de
                                    los responsables del procesamiento, encargado de la protección de datos de la
                                    empresa</a>
                                <div class="accordion-content">
                                    <ul class="list-descr-detall">
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                    </ul>
                                </div>
                            </li>
                            <li class="list-accordion">
                                <a class="accordion-heading font-bold"><i class="icon-cerrar"></i>La recopilación y el
                                    almacenamiento de datos de carácter personal, así como el tipo de datos y la
                                    finalidad de su uso</a>
                                <div class="accordion-content">
                                    <ul class="list-descr-detall">
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                    </ul>
                                </div>
                            </li>
                            <li class="list-accordion">
                                <a class="accordion-heading font-bold"><i class="icon-cerrar"></i>Divulgación de
                                    datos</a>
                                <div class="accordion-content">
                                    <ul class="list-descr-detall">
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                    </ul>
                                </div>
                            </li>
                            <li class="list-accordion">
                                <a class="accordion-heading font-bold"><i class="icon-cerrar"></i>Derechos de la persona
                                    afectada</a>
                                <div class="accordion-content">
                                    <ul class="list-descr-detall">
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                    </ul>
                                </div>
                            </li>
                            <li class="list-accordion">
                                <a class="accordion-heading font-bold"><i class="icon-cerrar"></i>Derecho de
                                    oposición</a>
                                <div class="accordion-content">
                                    <ul class="list-descr-detall">
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                    </ul>
                                </div>
                            </li>
                            <li class="list-accordion">
                                <a class="accordion-heading font-bold"><i class="icon-cerrar"></i>Data collection
                                    through the use of Google Analytics</a>
                                <div class="accordion-content">
                                    <ul class="list-descr-detall">
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                    </ul>
                                </div>
                            </li>
                            <li class="list-accordion">
                                <a class="accordion-heading font-bold"><i class="icon-cerrar"></i>Obligación de
                                    facilitar los datos</a>
                                <div class="accordion-content">
                                    <ul class="list-descr-detall">
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>
                                        <li class="item-list-descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum, voluptate expedita? Odit iusto totam quaerat at voluptatum maxime 
                                        veniam blanditiis cum rerum deserunt, facere quo doloremque sed illum, magnam soluta!</li>

                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

    </main>
<?php
    include 'src/includes/footer.php'
?>

</body>

</html>