<?php
    include 'src/includes/header.php'
?>
    <main class="main-products">
        <section class="sct-banner-products pos-rel" id="section0">
            <img src="assets/images/banner/product-bienestar.jpg" alt="" class="img-cover">
            <div class="container content-title-banner">
                <h3 class="title-peq-bproducts font-bold text-uppercase">Nos preocupamos</h3>
                <h2 class="title-b-products font-bold text-uppercase">por su bienestar</h2>
            </div>
        </section>
        <section class="sct-products container-fluid bg-productss">
            <div class="row">
                <div class="info-general-products col-xs-12 col-md-5 col-lg-4 animatedParent animateOnce" data-sequence='500'>
                    <!-- BREADCRUMB -->
                    <ol class="breadcrumb bread-products animated fadeInLeftShort" data-id="1">
                        <li class="item-bradcrumb"><a href="#" class="link-bradcrumb">Productos</a></li>
                        <li class="item-bradcrumb"><a href="#" class="link-bradcrumb color-bienestar active">Bienestar</a></li class="item-bradcrumb">
                    </ol>
                    <div class="wrapper-title-info t-bienestar animated fadeInLeftShort" data-id="2">
                        <i class="icon-t-info icon-bienestar"></i>
                        <h2 class="title-info">BIENESTAR</h2>
                    </div>
                    <p class="p-regular animated fadeInLeftShort" data-id="3">Los productos de la gama bienestar están al servicio de su bienestar general.
                        Aquí encontrará todo lo que necesita para llevar un estilo de vida moderno y sano, desde
                        productos térmicos, pasando por básculas de baño, balanzas de cocina y terapia de luz y de aire,
                        hasta aparatos de masaje y una serie completa de productos para el sueño y el descanso.</p>
                </div>
                <div class="info-card-products col-xs-12 col-md-7 col-lg-8 px-0">
                    <div class="container-fluid px-0">
                        <div class="animatedParent animateOnce" data-sequence='900'>
                            <!-- <div class="wrapper col-xs-4 px-0">
                                <div class="cols">
                                    <div class="col" ontouchstart="this.classList.toggle('hover');">
                                        <div class="containers">
                                            <div class="front"
                                                style="background-image: url(../../assets/images/card-products/sueño-y-descanso.jpg)">
                                                <div class="inner">
                                                    <h2>sueño <br>y descanso</h2>
                                                </div>
                                            </div>
                                            <div class="back" style="background-image: url(../../assets/images/card-products/sueño-y-descanso.jpg)">
                                                <div class="inner">
                                                    <div class="info-hp">
                                                        <h2>sueño <br>y descanso</h2>
                                                        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                                            Alias cum repellat velit quae suscipit c.</p>    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <a href="dispositivos-antirronquidos.php">
                                <div class="wrapper-card-info col-xs-12 col-sm-6 col-lg-4 px-0 animated fadeInLeftShort" data-id="1"
                                    style="background-image: url(assets/images/card-products/sueño-y-descanso.jpg)">
                                    <div class="info-card-p">
                                        <h2>SUEÑO <br>Y DESCANSO</h2>
                                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia rerum
                                            doloremque a ut modi facilis minus reiciendis. Voluptates, eaque corporis.</p>                                    
                                    </div>
                                    <div class="hover-card-product bg-bienestar"></div>
                                </div>
                            </a>
                            <a href="dispositivos-antirronquidos.php">
                                <div class="wrapper-card-info col-xs-12 col-sm-6 col-lg-4 px-0 animated fadeInLeftShort" data-id="2"
                                    style="background-image: url(assets/images/card-products/aire-y-aroma.jpg)">
                                    <div class="info-card-p">
                                        <h2>AIRE <br>Y AROMA</h2>
                                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia rerum
                                        doloremque a ut modi facilis minus reiciendis. Voluptates, eaque corporis.</p>                                    
                                    </div>
                                    <div class="hover-card-product bg-bienestar"></div>
                                </div>
                            </a>
                            <a href="dispositivos-antirronquidos.php">
                                <div class="wrapper-card-info col-xs-12 col-sm-6 col-lg-4 px-0 animated fadeInLeftShort" data-id="3"
                                    style="background-image: url(assets/images/card-products/products-termicos.jpg)">
                                    <div class="info-card-p">
                                        <h2>PRODUCTOS <br>TÉRMICOS</h2>
                                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia rerum
                                            doloremque a ut modi facilis minus reiciendis. Voluptates, eaque corporis.</p>                                    
                                    </div>
                                    <div class="hover-card-product bg-bienestar"></div>
                                </div>
                            </a>
                            <a href="dispositivos-antirronquidos.php">
                                <div class="wrapper-card-info col-xs-12 col-sm-6 col-lg-4 px-0 animated fadeInLeftShort" data-id="4"
                                    style="background-image: url(assets/images/card-products/balanzas-eq-coc.jpg)">
                                    <div class="info-card-p">
                                        <h2>BALANZAS DE <br>EQUIPAJE Y COCINA</h2>
                                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia rerum
                                            doloremque a ut modi facilis minus reiciendis. Voluptates, eaque corporis.</p>                                    
                                    </div>
                                    <div class="hover-card-product bg-bienestar"></div>
                                </div>
                            </a>
                            <a href="dispositivos-antirronquidos.php">
                                <div class="wrapper-card-info col-xs-12 col-sm-6 col-lg-4 px-0 animated fadeInLeftShort" data-id="5"
                                    style="background-image: url(assets/images/card-products/fototerapia.jpg)">
                                    <div class="info-card-p">
                                        <h2>FOTOTERAPIA</h2>
                                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia rerum
                                            doloremque a ut modi facilis minus reiciendis. Voluptates, eaque corporis.</p>                                    
                                    </div>
                                    <div class="hover-card-product bg-bienestar"></div>
                                </div>
                            </a>
                            <a href="dispositivos-antirronquidos.php">
                                <div class="wrapper-card-info col-xs-12 col-sm-6 col-lg-4 px-0 animated fadeInLeftShort" data-id="6"
                                    style="background-image: url(assets/images/card-products/shiatsu-y-masaje.jpg)">
                                    <div class="info-card-p">
                                        <h2>SHIATSU <br>Y MASAJE</h2>
                                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia rerum
                                            doloremque a ut modi facilis minus reiciendis. Voluptates, eaque corporis.</p>                                    
                                    </div>
                                    <div class="hover-card-product bg-bienestar"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <?php
        include 'src/includes/footer.php'
    ?>

</body>

</html>