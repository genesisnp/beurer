<?php
    include 'src/includes/header.php'
?>
    <main class="main-products">
        <section class="sct-banner-products pos-rel" id="section0">
            <img src="assets/images/banner/product-bienestar.jpg" alt="" class="img-cover">
            <div class="container content-title-banner">
                <h3 class="title-peq-bproducts font-bold text-uppercase">Nos preocupamos</h3>
                <h2 class="title-b-products font-bold text-uppercase">por su actividad</h2>
            </div>
        </section>
        <section class="sct-products container-fluid bg-productss">
            <div class="row">
                <div class="info-general-products col-xs-12 col-md-5 col-lg-4 animatedParent animateOnce" data-sequence='500'>
                    <!-- BREADCRUMB -->
                    <ol class="breadcrumb bread-products animated fadeInLeftShort" data-id="1">
                        <li class="item-bradcrumb"><a href="#" class="link-bradcrumb">Productos</a></li>
                        <li class="item-bradcrumb"><a href="#" class="link-bradcrumb color-actividad active">actividad</a></li class="item-bradcrumb">
                    </ol>
                    <div class="wrapper-title-info t-actividad animated fadeInLeftShort" data-id="2">
                        <i class="icon-t-info icon-actividad"></i>
                        <h2 class="title-info">actividad</h2>
                    </div>
                    <p class="p-regular animated fadeInLeftShort" data-id="3">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quas rerum ducimus sint unde odio maxime, nesciunt ut, 
                        iste soluta voluptates impedit aliquid provident eius excepturi omnis libero itaque pariatur beatae!.</p>
                </div>
                <div class="info-card-products col-xs-12 col-md-7 col-lg-8 px-0">
                    <div class="container-fluid px-0">
                        <div class="animatedParent animateOnce" data-sequence='900'>

                            <a href="balanzas-basicas.php">
                                <div class="wrapper-card-info col-xs-12 col-sm-6 col-lg-4 px-0 animated fadeInLeftShort" data-id="1"
                                    style="background-image: url(assets/images/card-products/sueño-y-descanso.jpg)">
                                    <div class="info-card-p">
                                        <h2>BALANZAS <br>BASICAS</h2>
                                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia rerum
                                            doloremque a ut modi facilis minus reiciendis. Voluptates, eaque corporis.</p>                                    
                                    </div>
                                    <div class="hover-card-product bg-actividad"></div>
                                </div>
                            </a>
                            <a href="balanzas-basicas.php">
                                <div class="wrapper-card-info col-xs-12 col-sm-6 col-lg-4 px-0 animated fadeInLeftShort" data-id="2"
                                    style="background-image: url(assets/images/card-products/aire-y-aroma.jpg)">
                                    <div class="info-card-p">
                                        <h2>BALANZAS <br>DE DIAGNOSTICO</h2>
                                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia rerum
                                            doloremque a ut modi facilis minus reiciendis. Voluptates, eaque corporis.</p>                                    
                                    </div>
                                    <div class="hover-card-product bg-actividad"></div>
                                </div>
                            </a>
                            <a href="balanzas-basicas.php">
                                <div class="wrapper-card-info col-xs-12 col-sm-6 col-lg-4 px-0 animated fadeInLeftShort" data-id="3"
                                    style="background-image: url(assets/images/card-products/products-termicos.jpg)">
                                    <div class="info-card-p">
                                        <h2>TRATAMIENTO <br>DEL DOLOR</h2>
                                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia rerum
                                            doloremque a ut modi facilis minus reiciendis. Voluptates, eaque corporis.</p>                                    
                                    </div>
                                    <div class="hover-card-product bg-actividad"></div>
                                </div>
                            </a>
                            <a href="balanzas-basicas.php">
                                <div class="wrapper-card-info col-xs-12 col-sm-6 col-lg-4 px-0 animated fadeInLeftShort" data-id="4"
                                    style="background-image: url(assets/images/card-products/balanzas-eq-coc.jpg)">
                                    <div class="info-card-p">
                                        <h2>PULSOMETRO</h2>
                                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia rerum
                                            doloremque a ut modi facilis minus reiciendis. Voluptates, eaque corporis.</p>                                    
                                    </div>
                                    <div class="hover-card-product bg-actividad"></div>
                                </div>
                            </a>
                            <a href="balanzas-basicas.php">
                                <div class="wrapper-card-info col-xs-12 col-sm-6 col-lg-4 px-0 animated fadeInLeftShort" data-id="5"
                                    style="background-image: url(assets/images/card-products/fototerapia.jpg)">
                                    <div class="info-card-p">
                                        <h2>CUIDADO <br>PEELING FACIAL</h2>
                                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia rerum
                                            doloremque a ut modi facilis minus reiciendis. Voluptates, eaque corporis.</p>
                                    </div>
                                    <div class="hover-card-product bg-actividad"></div>
                                </div>
                            </a>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <?php
        include 'src/includes/footer.php'
    ?>

</body>

</html>