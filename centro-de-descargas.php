<?php
    include 'src/includes/header.php'
?>
    <main class="main-us">
        <section class="sct-banner-int pos-rel" id="section0">
            <img src="assets/images/banner/centro-de-descargas.jpg" alt="" class="img-cover">
            <div class="container content-title-banner">
                <h1 class="title-banner font-nexaheavy text-uppercase">centro de descargas</h1>
            </div>
        </section>
        <section class="sct-download">
            <div class="container">
                <div class="row">
                    <!-- BREADCRUMB -->
                    <div class="col-xs-12">
                        <ol class="breadcrumb row">
                            <li class="item-bradcrumb"><a href="#" class="link-bradcrumb p-internas">Home</a></li>
                            <li class="item-bradcrumb"><a href="#" class="link-bradcrumb p-internas active">Centro de
                                    descargas</a></li class="item-bradcrumb">
                        </ol>
                    </div>
                    <!-- TITLE / DESCRIPTION PAGE -->
                    <div class="col-xs-12 descr-ttl-dob">
                        <div class="row animatedParent animateOnce" data-sequence='500'>
                            <div class="col-xs-12 col-sm-6 col-md-5 animated fadeInLeftShort" data-id="1">
                                <h2 class="title-border text-uppercase font-nexaheavy">las aplicaciones<br> beurer lo
                                    acompañan</h2>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-5 animated fadeInLeftShort" data-id="2">
                                <p class="p-internas">Cada vez es más común el uso de aplicaciones para realizar un
                                    seguimiento sobre la salud y la actividad física. La documentación permite
                                    visualizar el progreso conseguido y es una excelente fuente de motivación. En el
                                    centro de descargas puede descargar folletos y material informativo del producto o
                                    servicio que usted necesite</p>
                            </div>
                        </div>
                    </div>
                    <!--APP PARA ANDROID-->
                    <div class="col-xs-12 rsp-slct">
                        <div class="row animatedParent animateOnce" data-sequence="300">
                            <div class="col-xs-12 col-sm-5 col-md-3 animated fadeInLeftShort" data-id="1">
                                <div class="">
                                    <h3 class="titles-int-sl">apps<br> para android</h3>
                                    <p class="p-internas-sl">Aqui encontrará el archivo de descarga (.apk) de nuestras apps
                                        para Android.</p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-7 col-md-9 animated fadeIn" data-id="2">
                                <div class="row">
                                    <div class="col-xs-12 col-md-11 float-r">
                                        <div class="downdload-center slider-dscg">
                                            <div class="wrapper-cards-products">
                                                <div class="content-img-card">
                                                    <img src="assets/images/descargas/app1.jpg" alt="" class="img-cnt">
                                                </div>
                                                <div class="content-title-card">
                                                    <h2 class="h2-title font-nexaheavy">Health Manager 2.3</h2>
                                                    <span class="d-block subtitle-card">sauna facial</span>
                                                </div>
                                                <div class="btn-dscg">
                                                    <a download href="#" class="a-btn font-nexaheavy">descargar</a>
                                                </div>
                                                <a download href="#" class="icon-dscg-use icon-descargar"></a>
                                            </div>
                                            <div class="wrapper-cards-products">
                                                <div class="content-img-card">
                                                    <img src="assets/images/descargas/app1.jpg" alt="" class="img-cnt">
                                                </div>
                                                <div class="content-title-card">
                                                    <h2 class="h2-title font-nexaheavy">BodShape V1.1.2</h2>
                                                    <span class="d-block subtitle-card">sauna facial</span>
                                                </div>
                                                <div class="btn-dscg">
                                                    <a download href="#" class="a-btn font-nexaheavy">descargar</a>
                                                </div>
                                                <a download href="#" class="icon-dscg-use icon-descargar"></a>
                                            </div>
                                            <div class="wrapper-cards-products">
                                                <div class="content-img-card">
                                                    <img src="assets/images/descargas/app2.jpg" alt="" class="img-cnt">
                                                </div>
                                                <div class="content-title-card">
                                                    <h2 class="h2-title font-nexaheavy">SleepQiet 1.4</h2>
                                                    <span class="d-block subtitle-card">sauna facial</span>
                                                </div>
                                                <div class="btn-dscg">
                                                    <a download href="#" class="a-btn font-nexaheavy">descargar</a>
                                                </div>
                                                <a download href="#" class="icon-dscg-use icon-descargar"></a>
                                            </div>
                                            <div class="wrapper-cards-products">
                                                <div class="content-img-card">
                                                    <img src="assets/images/descargas/app3.jpg" alt="" class="img-cnt">
                                                </div>
                                                <div class="content-title-card">
                                                    <h2 class="h2-title font-nexaheavy">eurer MyIPL V1.1</h2>
                                                    <span class="d-block subtitle-card">sauna facial</span>
                                                </div>
                                                <div class="btn-dscg">
                                                    <a download href="#" class="a-btn font-nexaheavy">descargar</a>
                                                </div>
                                                <a download href="#" class="icon-dscg-use icon-descargar"></a>
                                            </div>
                                            <div class="wrapper-cards-products">
                                                <div class="content-img-card">
                                                    <img src="assets/images/descargas/app2.jpg" alt="" class="img-cnt">
                                                </div>
                                                <div class="content-title-card">
                                                    <h2 class="h2-title font-nexaheavy">SleepQiet 1.4</h2>
                                                    <span class="d-block subtitle-card">sauna facial</span>
                                                </div>
                                                <div class="btn-dscg">
                                                    <a download href="#" class="a-btn font-nexaheavy">descargar</a>
                                                </div>
                                                <a download href="#" class="icon-dscg-use icon-descargar"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--SOFTWARE BEURER-->
                    <div class="col-xs-12 rsp-slct">
                        <div class="row animatedParent animateOnce" data-sequence="300">
                            <div class="col-xs-12 col-sm-5 col-md-3 animated fadeInLeftShort" data-id="1">
                                <h3 class="titles-int-sl">software<br> beurer</h3>
                                <p class="p-internas-sl">Aqui encontrará las últimas versiones de software de nuestros
                                    productos.</p>
                            </div>
                            <div class="col-xs-12 col-sm-7 col-md-9 animated fadeIn" data-id="2">
                                <div class="row">
                                    <div class="col-xs-12 col-md-11 float-r">
                                        <div class="downdload-center slider-dscg">
                                            <div class="wrapper-cards-products">
                                                <div class="content-img-card">
                                                    <img src="assets/images/descargas/app1.jpg" alt="" class="img-cnt">
                                                </div>
                                                <div class="content-title-card">
                                                    <h2 class="h2-title font-nexaheavy">Health Manager 2.3</h2>
                                                    <span class="d-block subtitle-card">sauna facial</span>
                                                </div>
                                                <div class="btn-dscg">
                                                    <a download href="#" class="a-btn font-nexaheavy">descargar</a>
                                                </div>
                                                <a download href="#" class="icon-dscg-use icon-descargar"></a>
                                            </div>
                                            <div class="wrapper-cards-products">
                                                <div class="content-img-card">
                                                    <img src="assets/images/descargas/app1.jpg" alt="" class="img-cnt">
                                                </div>
                                                <div class="content-title-card">
                                                    <h2 class="h2-title font-nexaheavy">BodShape V1.1.2</h2>
                                                    <span class="d-block subtitle-card">sauna facial</span>
                                                </div>
                                                <div class="btn-dscg">
                                                    <a download href="#" class="a-btn font-nexaheavy">descargar</a>
                                                </div>
                                                <a download href="#" class="icon-dscg-use icon-descargar"></a>
                                            </div>
                                            <div class="wrapper-cards-products">
                                                <div class="content-img-card">
                                                    <img src="assets/images/descargas/app2.jpg" alt="" class="img-cnt">
                                                </div>
                                                <div class="content-title-card">
                                                    <h2 class="h2-title font-nexaheavy">SleepQiet 1.4</h2>
                                                    <span class="d-block subtitle-card">sauna facial</span>
                                                </div>
                                                <div class="btn-dscg">
                                                    <a download href="#" class="a-btn font-nexaheavy">descargar</a>
                                                </div>
                                                <a download href="#" class="icon-dscg-use icon-descargar"></a>
                                            </div>
                                            <div class="wrapper-cards-products">
                                                <div class="content-img-card">
                                                    <img src="assets/images/descargas/app3.jpg" alt="" class="img-cnt">
                                                </div>
                                                <div class="content-title-card">
                                                    <h2 class="h2-title font-nexaheavy">eurer MyIPL V1.1</h2>
                                                    <span class="d-block subtitle-card">sauna facial</span>
                                                </div>
                                                <div class="btn-dscg">
                                                    <a download href="#" class="a-btn font-nexaheavy">descargar</a>
                                                </div>
                                                <a download href="#" class="icon-dscg-use icon-descargar"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--CATÁLOGO BEURER-->
                    <div class="col-xs-12 rsp-slct">
                        <div class="row animatedParent animateOnce" data-sequence="300">
                            <div class="col-xs-12 col-sm-5 col-md-3 animated fadeInLeftShort" data-id="1">
                                <h3 class="titles-int-sl">catálogo<br> beurer</h3 >
                                <p class="p-internas-sl">Aqui podrá descargar nuestros catálogos actual data-id="2"es.</p>
                            </div>
                            <div class="col-xs-12 col-sm-7 col-md-9 animated fadeIn" data-id="2">
                                <div class="row">
                                    <div class="col-xs-12 col-md-11 float-r">
                                        <div class="downdload-center slider-dscg">
                                            <div class="wrapper-cards-products">
                                                <div class="content-img-card">
                                                    <img src="assets/images/descargas/catalog.jpg" alt=""
                                                        class="img-cnt">
                                                </div>
                                                <div class="content-title-card">
                                                    <h2 class="h2-title font-nexaheavy">Health Manager 2.3</h2>
                                                </div>
                                                <div class="btn-dscg">
                                                    <a download href="#" class="a-btn font-nexaheavy">descargar</a>
                                                </div>
                                                <a download href="#" class="icon-dscg-use icon-descargar"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--OTROS-->
                    <div class="col-xs-12 rsp-slct">
                        <div class="row animatedParent animateOnce" data-sequence="300">
                            <div class="col-xs-12 col-sm-5 col-md-3 animated fadeInLeftShort" data-id="1">
                                <h3 class="titles-int-sl">otros</h3>
                                <p class="p-internas-sl">Aqui encontrará otros docuemntos e información importantes.</p>
                            </div>
                            <div class="col-xs-12 col-sm-7 col-md-9 animated fadeIn" data-id="2">
                                <div class="row">
                                    <div class="col-xs-12 col-md-11 float-r">
                                        <div class="downdload-center slider-dscg">
                                            <div class="wrapper-cards-products">
                                                <div class="content-img-card">
                                                    <img src="assets/images/descargas/otros1.jpg" alt=""
                                                        class="img-cnt">
                                                </div>
                                                <div class="content-title-card">
                                                    <h2 class="h2-title font-nexaheavy">Health Manager 2.3</h2>
                                                </div>
                                                <div class="btn-dscg">
                                                    <a download href="#" class="a-btn font-nexaheavy">descargar</a>
                                                </div>
                                                <a download href="#" class="icon-dscg-use icon-descargar"></a>
                                            </div>
                                            <div class="wrapper-cards-products">
                                                <div class="content-img-card">
                                                    <img src="assets/images/descargas/otros2.jpg" alt=""
                                                        class="img-cnt">
                                                </div>
                                                <div class="content-title-card">
                                                    <h2 class="h2-title font-nexaheavy">BodShape V1.1.2</h2>
                                                </div>
                                                <div class="btn-dscg">
                                                    <a download href="#" class="a-btn font-nexaheavy">descargar</a>
                                                </div>
                                                <a download href="#" class="icon-dscg-use icon-descargar"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>
    <?php
        include 'src/includes/footer.php'
    ?>

</body>

</html>