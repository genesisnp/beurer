<?php
    include 'src/includes/header.php'
?>
    <main class="main-detail-products">
        <section class="sct-detail-products">
            <div class="container-fluid">
                <div class="row">
                    <!-- BREADCRUMB -->
                    <div class="col-xs-12">
                        <ol class="breadcrumb row">
                            <li class="item-bradcrumb"><a href="#" class="link-bradcrumb p-internas">Productos</a></li>
                            <li class="item-bradcrumb"><a href="#" class="link-bradcrumb p-internas">Actividad</a></li>
                            <li class="item-bradcrumb"><a href="#" class="link-bradcrumb p-internas">Deporte y
                                    actividad</a></li>
                            <li class="item-bradcrumb"><a href="#" class="link-bradcrumb p-internas">Sensores de
                                    actividad</a></li>
                            <li class="item-bradcrumb"><a href="#" class="link-bradcrumb p-internas active">AS 95</a>
                            </li class="item-bradcrumb">
                        </ol>
                    </div>
                    <div class="col-xs-12 col-md-7">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="tabss row">
                                    <a href="#" class="icon-descarga-detalle"></a>
                                    <div class="tabs_gotoWrap col-xs-12 col-md-2 animatedParent animateOnce" data-sequence='500'>
                                        <div class="animated fadeInLeftShort tabs_goto -active" data-id="1"><img class="img-cover" src="assets/images/productos/490.jpg" alt=""></div>
                                        <div class="animated fadeInLeftShort tabs_goto" data-id="2"><img class="img-cover" src="assets/images/productos/producto2.jpg" alt=""></div>
                                        <div class="animated fadeInLeftShort tabs_goto" data-id="3"><img class="img-cover" src="assets/images/productos/prod-rel.jpg" alt=""></div>
                                        <div class="animated fadeInLeftShort video-circle" data-id="4">
                                            <a data-fancybox href="assets/video/video_1.mp4">
                                                <img class="img-cover" src="assets/images/icons/play.svg" />
                                            </a>
                                        </div>
                                    </div>
                                    <div class="tabs_sectionsWrap col-xs-12 col-md-10 animatedParent animateOnce">
                                        <section class="tabs_section -open animated growIn"><img src="assets/images/productos/490.jpg" alt=""></section>
                                        <section class="tabs_section animated growIn"><img src="assets/images/productos/producto2.jpg" alt=""></section>
                                        <section class="tabs_section animated growIn"><img src="assets/images/productos/prod-rel.jpg" alt=""></section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-5 info-prod animatedParent animateOnce">
                        <div class="animated fadeInRightShort">
                            <button class="trans-info name-product bg-actividad font-nexaregular">Actividad</button>
                            <h2 class="px-0 col-xs-12 title-detail-product font-nexaheavy">Sensor de actividad de Beurer
                                - AS
                                95 Pulse Bluetooth</h2>
                            <span class="px-0 col-xs-12 subtitle-detail-product font-nexaregular">Salud a tráves del
                                movimiento</span>
                            <p class="p-internas">¡El acompañante diario ideal! El rastreador de actividad registra sus
                                datos de movimiento y su sueño. Además, podrá medir cuando lo desee su pulso a través
                                del sensor óptico integrado. Asimismo, recibirá en la pantalla notificaciones directas
                                de llamadas, SMS y mensajes.</p>
                            <h3 class="ttl-encuent font-nexaheavy text-center bg-primary">ENCUÉNTRALO EN:</h3>
                            <div class="col-xs-12 logos-tiendas">
                                <figure class="col-xs-4 col-sm-3"><img src="assets/images/logos/beurer-.jpg" alt=""></figure>
                                <figure class="col-xs-4 col-sm-3"><img src="assets/images/logos/falabella.jpg" alt=""></figure>
                                <figure class="col-xs-4 col-sm-3"><img src="assets/images/logos/hiraoka.jpg" alt=""></figure>
                                <figure class="col-xs-4 col-sm-3"><img src="assets/images/logos/aruma.jpg" alt=""></figure>

                            </div>
                            <div class="tabs-info-product col-xs-12 px-0">
                                <div class="tabs text-uppercase">
                                    <span class="font-regular tab-link current"
                                        data-tab="description">Descripción</span>
                                    <span class="font-regular tab-link" data-tab="ficha-tecnica">ficha técnica</span>
                                    <span class="font-regular tab-link" data-tab="accesorios">accesorios</span>
                                </div>

                                <div class="content">
                                    <div id="description" class="tab-content current">
                                        <ul class="list-descr-detall">
                                            <li class="item-list-descr">Registro de actividad y sueño y transmisión de
                                                los datos al smartphone.</li>
                                            <li class="item-list-descr">Medición del pulso en la muñeca mediante un
                                                sensor óptico.</li>
                                            <li class="item-list-descr">Notificación de llamadas, SMS y mensajes.</li>
                                            <li class="item-list-descr">Seguimiento de la actividad: cantidad de pasos,
                                                tramo recorrido, cálculo del consumo de calorías, duración de la
                                                actividad y consecución del objetivo diario de movimiento.</li>
                                        </ul>
                                    </div>
                                    <div id="ficha-tecnica" class="tab-content">
                                        <div class="descr-ft">
                                            <p class="text-primary-ft d-in-block">Denominación del producto</p>
                                            <p class="text-secondary-ft d-in-block">Sensor de actividad</p>
                                        </div>
                                        <div class="descr-ft">
                                            <p class="text-primary-ft d-in-block">Connect</p>
                                            <p class="text-secondary-ft d-in-block">Sí</p>
                                        </div>
                                        <div class="descr-ft">
                                            <p class="text-primary-ft d-in-block">Funcionamiento con batería</p>
                                            <p class="text-secondary-ft d-in-block">Sí</p>
                                        </div>
                                        <div class="descr-ft">
                                            <p class="text-primary-ft d-in-block">Indicación de notificaciones
                                                (WhatsApp, etc.)</p>
                                            <p class="text-secondary-ft d-in-block">Sí</p>
                                        </div>
                                        <div class="descr-ft">
                                            <p class="text-primary-ft d-in-block">Indicación de llamadas entrantes</p>
                                            <p class="text-secondary-ft d-in-block">Sí</p>
                                        </div>
                                    </div>
                                    <div id="accesorios" class="tab-content">
                                        <div class="container-fluid px-0">
                                            <div class="row">
                                                <div class="col-xs-3 acces">
                                                    <div class="img-acces">
                                                        <a href="assets/images/productos/producto1.jpg"
                                                            data-fancybox="images">
                                                            <img src="assets/images/productos/producto1.jpg" alt="">
                                                        </a>
                                                    </div>
                                                    <h2 class="ttl-prod-acces font-nexaregular">Accesorio 1</h2>
                                                </div>
                                                <div class="col-xs-3 acces">
                                                    <div class="img-acces">
                                                        <a href="assets/images/productos/producto1.jpg"
                                                            data-fancybox="images">
                                                            <img src="assets/images/productos/producto2.jpg" alt="">
                                                        </a>
                                                    </div>
                                                    <h2 class="ttl-prod-acces font-nexaregular">Accesorio 1</h2>
                                                </div>
                                                <div class="col-xs-3 acces">
                                                    <div class="img-acces">
                                                        <a href="assets/images/productos/producto1.jpg"
                                                            data-fancybox="images">
                                                            <img src="assets/images/productos/producto1.jpg" alt="">
                                                        </a>
                                                    </div>
                                                    <h2 class="ttl-prod-acces font-nexaregular">Accesorio 1</h2>
                                                </div>
                                                <div class="col-xs-3 acces">
                                                    <div class="img-acces">
                                                        <a href="assets/images/productos/producto1.jpg"
                                                            data-fancybox="images">
                                                            <img src="assets/images/productos/producto2.jpg" alt="">
                                                        </a>
                                                    </div>
                                                    <h2 class="ttl-prod-acces font-nexaregular">Accesorio 1</h2>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- PRODUCTOS RELACIONADOS -->
        <section class="sct-product-rel container-fluid">
            <div class="row">
                <h1 class="ttl-prl font-bold text-center">PRODUCTOS QUE TE PUEDEN INTERESAR</h1>
                <div class="carrosuel-two-home">
                    <div class="wrapper-cards-products">
                        <div class="content-img-card">
                            <img src="assets/images/productos/prod-rel.jpg" alt="" class="img-cnt">
                        </div>
                        <div class="content-title-card">
                            <h2 class="h2-title font-nexaheavy">FC 96 pureo intense cleansing</h2>
                            <span class="d-block subtitle-card">sauna facial</span>
                            <div class="btn-vp">
                                <a href="detalle-de-producto.php" class="a-btn font-nexaheavy">ver producto</a>
                            </div>
                        </div>
                    </div>
                    <div class="wrapper-cards-products">
                        <div class="content-img-card">
                            <img src="assets/images/productos/prod-rel.jpg" alt="" class="img-cnt">
                        </div>
                        <div class="content-title-card">
                            <h2 class="h2-title font-nexaheavy">FC 96 pureo intense cleansing</h2>
                            <span class="d-block subtitle-card">sauna facial</span>
                            <div class="btn-vp">
                                <a href="detalle-de-producto.php" class="a-btn font-nexaheavy">ver producto</a>
                            </div>
                        </div>
                    </div>
                    <div class="wrapper-cards-products">
                        <div class="content-img-card">
                            <img src="assets/images/productos/prod-rel.jpg" alt="" class="img-cnt">
                        </div>
                        <div class="content-title-card">
                            <h2 class="h2-title font-nexaheavy">FC 96 pureo intense cleansing</h2>
                            <span class="d-block subtitle-card">sauna facial</span>
                            <div class="btn-vp">
                                <a href="detalle-de-producto.php" class="a-btn font-nexaheavy">ver producto</a>
                            </div>
                        </div>
                    </div>
                    <div class="wrapper-cards-products">
                        <div class="content-img-card">
                            <img src="assets/images/productos/prod-rel.jpg" alt="" class="img-cnt">
                        </div>
                        <div class="content-title-card">
                            <h2 class="h2-title font-nexaheavy">FC 96 pureo intense cleansing</h2>
                            <span class="d-block subtitle-card">sauna facial</span>
                            <div class="btn-vp">
                                <a href="detalle-de-producto.php" class="a-btn font-nexaheavy">ver producto</a>
                            </div>
                        </div>
                    </div>
                    <div class="wrapper-cards-products">
                        <div class="content-img-card">
                            <img src="assets/images/productos/prod-rel.jpg" alt="" class="img-cnt">
                        </div>
                        <div class="content-title-card">
                            <h2 class="h2-title font-nexaheavy">FC 96 pureo intense cleansing</h2>
                            <span class="d-block subtitle-card">sauna facial</span>
                            <div class="btn-vp">
                                <a href="detalle-de-producto.php" class="a-btn font-nexaheavy">ver producto</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/libraries/fancybox.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.tabs span').click(function () {
                var tab_id = $(this).attr('data-tab');

                $('.tabs span').removeClass('current');
                $('.tab-content').removeClass('current');

                $(this).addClass('current');
                $("#" + tab_id).addClass('current');
            })
        })
    </script>
    <script>
        $(".tabs_goto").click(function() {
            $(this)
                .addClass("-active")
                .siblings()
                .removeClass("-active")
                .closest(".tabss")
                .find(".tabs_section")
                .eq($(this).index())
                .addClass("-open")
                .siblings()
                .removeClass("-open")
            })
    </script>
</body>

</html>